# docs

PeopleFlow test group documentation

## Just run

-Spring Boot Read Service  
-Spring Boot Write Service  
-Spring Cloud Config Server  
-PostgreSQL database

```
docker-compose up -d
```

## Authorize
Http header with jwt token required for secure endpoints (placed into Postman collection) :  

Authorization: Bearer <jwt-token>

```
{
  "sub": "999",
  "user_name": "unknown_manager",
  "roles": [
    "manager_role"
  ],
  "iss": "peopleflow-auth",
  "client_id": "peopleflow-web",
  "aud": [
    "employee-management"
  ],
  "scope": [
    "openid"
  ],
  "exp": 2620022672,
  "iat": 1620019072,
  "jti": "6cfe8c5f-1ef2-4127-b38b-10137964d0c6"
}
```
```
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5OTkiLCJ1c2VyX25hbWUiOiJ1bmtub3duX21hbmFnZXIiLCJyb2xlcyI6WyJtYW5hZ2VyX3JvbGUiXSwiaXNzIjoicGVvcGxlZmxvdy1hdXRoIiwiY2xpZW50X2lkIjoicGVvcGxlZmxvdy13ZWIiLCJhdWQiOlsiZW1wbG95ZWUtbWFuYWdlbWVudCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoyNjIwMDIyNjcyLCJpYXQiOjE2MjAwMTkwNzIsImp0aSI6IjZjZmU4YzVmLTFlZjItNDEyNy1iMzhiLTEwMTM3OTY0ZDBjNiJ9.NmfYqKrUgRBMYdbFCh4aPA4NuOO2eWazjGM76-mNUMO7LPvr5gz9Y903P5fEhYnMqlKNCqwNUSyUMa4GgsIf0tEhnYYZDOfoQCdStwvh_3yIBzr_Rc4yMMhcA1dNWcOq96U9I7xVZgVGWHYuGKuRdfy6ziDvdYhxG3-4npEtcisUArqwtSLseylJpmrxmAKgFrV9g6Rd738HF-WjAS4YDzij1xoRVJODXc9gzK8VfLwH7VXlxx0DOQE6ieAC0CannHNtJ8_mQtvkmaiSdPWmZz-CAod-xYnQWeQflf5iqq6NT8HIMa4X3-CwIG12SjIOgy1WOgQ03gXmVUUIwr4Ong
```

## Swagger documentation

http://localhost:8080/swagger-ui.html

## Postman collection
https://www.getpostman.com/collections/501177473ab564ec4630

## Features

- native PostgreSQL DB in docker container runs with integration tests
- annotation based swagger ui documentation available
- Spring Cloud Config provides server-side and client-side support for externalized configuration in a distributed system.
  It pulls configuration for microservices clients from a git repository
- http request/response logging
- name of authorized user is in MDC attributes and shown in logs
- internationalized messages depend on Accept-Language http header
- hibernate with jsonb type supporting
- externalized endpoints access policies config via .yaml files
- logs tracing through all microservices
- Spring State Machine manages employee states transitions